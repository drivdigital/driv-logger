'use strict';

const os = require( 'os' );
const path = require( 'path' );
const fs = require( 'fs' );
const fecha = require( 'fecha' );

const defaultOptions = require( './default.options' );

class Logger {

    /**
     * @constructor
     * @param {Object} options
     */
    constructor( options = null ) {

        // Set up the options.
        let opt = Object.assign( {}, defaultOptions );
        if ( options ) {
            opt = Object.assign( opt, options );
        }

        /**
         * Array of core appenders.
         * @private
         * @type {Array}
         */
        this.coreAppenders = [
            'appender/console',
            'appender/file'
        ];

        /**
         * Configuration object.
         * @private
         * @type {Object}
         */
        this.options = opt;

        /**
         * Log level
         * @private
         * @type {Number}
         */
        this.level = this.resolveLevel( this.options.level );

        /**
         * Date format
         * @private
         * @type {String}
         */
        this.date_format = this.options.date_format;

        /**
         * Layout
         * @private
         * @type {String}
         */
        this.layout = this.options.layout;

        /**
         * Array of loaded appender configurations.
         * @private
         * @type {Array}
         */
        this.appenders = [];

        /**
         * Array of logged messages.
         * @private
         * @type {Array}
         */
        this.messages = [];

        // Load the appenders from the options.
        this.options.appenders.map( ( appenderConfig )=> {
            this.loadAppender( appenderConfig );
        } );
    }

    /**
     * Outputs a trace message to appender.
     * @param {*} message
     * @param {*} messageN
     */
    trace( message, ...messageN ) {
        this.append( Logger.LogLevel.TRACE, message, messageN );
    }

    /**
     * Outputs a debug message to appender.
     * @param {*} message
     * @param {*} messageN
     */
    debug( message, ...messageN ) {
        this.append( Logger.LogLevel.DEBUG, message, messageN );
    }

    /**
     * Outputs an info message to appender.
     * @param {*} message
     * @param {*} messageN
     */
    info( message, ...messageN ) {
        this.append( Logger.LogLevel.INFO, message, messageN );
    }

    /**
     * Outputs a warn message to appender.
     * @param {*} message
     * @param {*} messageN
     */
    warn( message, ...messageN ) {
        this.append( Logger.LogLevel.WARN, message, messageN );
    }

    /**
     * Outputs an error message to appender.
     * @param {*} message
     * @param {*} messageN
     */
    error( message, ...messageN ) {
        this.append( Logger.LogLevel.ERROR, message, messageN );
    }

    /**
     * Outputs a fatal message to appender.
     * @param {*} message
     * @param {*} messageN
     */
    fatal( message, ...messageN ) {
        this.append( Logger.LogLevel.FATAL, message, messageN );
    }

    /**
     * @param {Logger.LogLevel|String} level
     * @param {String} appenderType
     * @todo: test
     */
    setLevel( level, appenderType = null ) {
        let lev;
        if ( typeof level == 'object' && level.level ) {
            lev = level.level;
        }
        else {
            lev = this.resolveLevel( level );
        }

        if ( appenderType ) {
            this.setAppenderProp( appenderType, 'level', level );
            return;
        }

        this.level = lev;
    }

    /**
     * Set the date format.
     * @param {String} dateFormat
     * @param {String} appenderType
     * @todo: test
     */
    setDateFormat( dateFormat, appenderType = null ) {
        if ( appenderType ) {
            this.setAppenderProp( appenderType, 'date_format', dateFormat );
            return;
        }
        this.date_format = dateFormat;
    }

    /**
     * Set the layout.
     * @param {String} layout
     * @param {String} appenderType
     */
    setLayout( layout, appenderType = null ) {
        if ( appenderType ) {
            this.setAppenderProp( appenderType, 'layout', layout );
            return;
        }
        this.layout = layout;
    }

    /**
     * @private
     * @param appenderType
     * @param prop
     * @param value
     */
    setAppenderProp( appenderType, prop, value ) {
        for ( let a of this.appenders ) {
            if ( a.config.type == appenderType ) {
                a.config['prop'] = value;
                break;
            }
        }
    }

    /**
     * Returns an array of appender configurations.
     * @returns {Array}
     */
    getAppenderConfig() {
        let result = [];
        for ( let a of this.appenders ) {
            result.push( a.config );
        }
        return result;
    }

    /**
     * Loads an appender.
     * @param appenderConfig
     */
    loadAppender( appenderConfig ) {
        if ( !appenderConfig.type ) {
            throw new Error( 'Appender must have a type' );
        }

        if ( this.appenderIsLoaded( appenderConfig.type ) ) {
            return;
        }

        let resolvedPath = this.resolveAppenderPath( appenderConfig.type );
        try {
            // Try to load with the resolved type path.
            try {
                this.appenders.push( {
                    "module": require( resolvedPath ),
                    "config": appenderConfig
                } );
            }
            catch ( ex ) {
                // Try to load with the raw type path.
                this.appenders.push( {
                    "module": require( appenderConfig.type ),
                    "config": appenderConfig
                } );
            }
        }
        catch ( ex ) {
            throw new Error( ex.code + ' - ' + appenderConfig.type.toString() );
        }
    }

    /**
     * @param {String} appenderType
     * @returns {boolean}
     */
    appenderIsLoaded( appenderType ) {
        for ( let a of this.appenders ) {
            if ( a.config.type == appenderType ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Removes an appender from the logger instance.
     * @param {String} typeToRemove
     */
    removeAppender( typeToRemove ) {
        this.appenders = this.appenders.filter( ( appender )=> {
            if ( typeToRemove == appender.config.type ) {
                const name = require.resolve( this.resolveAppenderPath( appender.config.type ) );
                delete require.cache[name];
            }

            return appender.config.type != typeToRemove;
        } );
    }

    /**
     * Returns the level based on the level.text.
     * Returns ALL.level if levelText is not found.
     * @private
     * @param levelText
     * @returns {Number}
     */
    resolveLevel( levelText ) {
        for ( let key in Logger.LogLevel ) {
            if ( !Logger.LogLevel.hasOwnProperty( key ) ) {
                continue;
            }
            let level = Logger.LogLevel[key];
            if ( levelText.toUpperCase() == level.text ) {
                return level.level;
            }
        }

        return Logger.LogLevel.ALL.level;
    }

    /**
     * @private
     * @param {String} layout
     * @param {String} dateFormat
     * @param {*} level
     * @param {*} message
     * @param {Array} messageN
     */
    parseLayout( layout, dateFormat, level, message, messageN ) {
        let date = fecha.format( new Date(), dateFormat );
        let messages = [];
        messages.push( this.parseMessage( message ) );
        if ( messageN ) {
            messageN.map( mn => messages.push( this.parseMessage( mn ) ) );
        }

        return layout
            .replace( /%d/, date )
            .replace( /%c/, level.text )
            .replace( /%h/, os.hostname() )
            .replace( /%m/, messages.join( ' ' ) );
    }

    /**
     * @private
     * @param {*} message
     * @returns {*}
     */
    parseMessage( message ) {
        return typeof message == 'object' ? JSON.stringify( message ) : message
    }

    /**
     * @private
     * @param {*} level
     * @param {*} message
     * @param {*} messageN
     */
    append( level, message, messageN ) {
        if ( !this.shouldAppend( level, this.level ) ) {
            return;
        }

        this.appenders.map( ( appender )=> {
            if ( appender.config.level ) {
                if ( !this.shouldAppend( level, this.resolveLevel( appender.config.level ) ) ) {
                    return;
                }
            }

            let layout = this.layout;
            if ( appender.config.layout ) {
                layout = appender.config.layout;
            }

            let dateFormat = this.date_format;
            if ( appender.config.date_format ) {
                dateFormat = appender.config.date_format;
            }

            let m = this.parseLayout( layout, dateFormat, level, message, messageN );

            appender.module.appender( m, appender.config.options );
        } );
        this.messages.push( message );
    }

    /**
     * @private
     * @param {*} level
     * @param {Number} maxLevel
     * @returns {boolean}
     */
    shouldAppend( level, maxLevel ) {
        const offLevel = Logger.LogLevel.OFF.level;
        if ( maxLevel == offLevel || level.level == offLevel ) {
            return false;
        }
        return level.level <= maxLevel;
    }

    /**
     * @private
     * @param {String} appenderType
     * @returns {String}
     */
    resolveAppenderPath( appenderType ) {
        let result;
        if ( this.coreAppenders.indexOf( appenderType ) > -1 ) {
            result = './' + appenderType;
        }
        else {
            result = path.normalize( path.dirname( module.parent.filename ) + '/' + appenderType );
        }

        return result;
    }

}

Logger.LogLevel = {
    OFF: {
        level: 0,
        text: 'OFF'
    },
    FATAL: {
        level: 100,
        text: 'FATAL'
    },
    ERROR: {
        level: 200,
        text: 'ERROR'
    },
    WARN: {
        level: 300,
        text: 'WARN'
    },
    INFO: {
        level: 400,
        text: 'INFO'
    },
    DEBUG: {
        level: 500,
        text: 'DEBUG'
    },
    TRACE: {
        level: 600,
        text: 'TRACE'
    },
    ALL: {
        level: Number.MAX_SAFE_INTEGER,
        text: 'TRACE'
    }
};

module.exports = Logger;