'use strict';

const fs = require( 'fs' );

exports.appender = function ( message, options ) {
    const logStream = fs.createWriteStream( options.path, {'flags': 'a'} );
    logStream.end( message + '\n' );
};