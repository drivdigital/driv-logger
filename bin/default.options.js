module.exports = {
    /**
     * ALL
     * TRACE
     * DEBUG
     * INFO
     * LogLevel
     * ERROR
     * FATAL
     * OFF
     */
    "level": "ALL",
    /**
     * date format
     * See: http://momentjs.com/docs/#/displaying/format/
     */
    "date_format": "YYYY-MM-DDTHH:mm:ss.SSS",
    /**
     * layout:
     *  %d - date
     *  %c - log level text
     *  %h - hostname
     *  %m - message
     */
    "layout": "[%d] [%c] %m",
    /**
     * Appender types
     *  console
     *  file
     */
    "appenders": [
        {
            "type": "appender/console"
        }
        // {
        //     "type": "appender/file",
        //     "options": {
        //         "path": "driv-logger.log"
        //     }
        //
        // }
    ]
};