'use strict';

const chai = require( 'chai' );
const assert = chai.assert;
const expect = chai.expect;
const sinon = require( 'sinon' );

const Logger = require( '../bin' );

describe( 'Default options', function () {
    const logger = new Logger();

    it( 'should have expected default #coreAppenders', function () {
        expect( logger.coreAppenders ).to.have.members( ['appender/console', 'appender/file'] );
    } );

    it( 'should have the expected default #level', function () {
        expect( logger.level ).to.equal( Logger.LogLevel.ALL.level );
    } );

    it( 'should have the expected default #date_format', function() {
        expect( logger.date_format ).to.equal( "YYYY-MM-DDTHH:mm:ss.SSS" );
    } );

    it( 'should have the expected default #layout', function() {
        expect( logger.layout ).to.equal( "[%d] [%c] %m" );
    } );

    it( 'should have the expected default #appenders', function() {
        expect( logger.getAppenderConfig() ).to.deep.equal( [
            {
                "type": "appender/console"
            }
        ] );
    } );

} );