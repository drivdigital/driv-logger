'use strict';

const chai = require( 'chai' );
const assert = chai.assert;
const expect = chai.expect;
const sinon = require( 'sinon' );

const Logger = require( '../bin' );

describe( 'Log level', function () {
    const logger = new Logger();

    const offLevel = Logger.LogLevel.OFF.level;
    const fatalLevel = Logger.LogLevel.FATAL.level;
    const errorLevel = Logger.LogLevel.ERROR.level;
    const warningLevel = Logger.LogLevel.WARN.level;
    const infoLevel = Logger.LogLevel.INFO.level;
    const debugLevel = Logger.LogLevel.DEBUG.level;
    const traceLevel = Logger.LogLevel.TRACE.level;
    const allLevel = Logger.LogLevel.ALL.level;

    it( 'OFF level should be 0', function () {
        expect( offLevel ).to.equal( 0 );
    } );

    it( 'ERROR level should be above FATAL level', function () {
        assert.isAbove( errorLevel, fatalLevel );
    } );

    it( 'WARN level should be above ERROR level', function () {
        assert.isAbove( warningLevel, errorLevel );
    } );

    it( 'INFO level should be above WARN level', function () {
        assert.isAbove( infoLevel, warningLevel );
    } );

    it( 'DEBUG level should be above INFO level', function () {
        assert.isAbove( debugLevel, infoLevel );
    } );

    it( 'TRACE level should be above DEBUG level', function () {
        assert.isAbove( traceLevel, debugLevel );
    } );

    it( 'ALL level should be above TRACE level', function () {
        assert.isAbove( allLevel, traceLevel );
    } );

    it( 'ALL level should be MAX_SAFE_INTEGER', function () {
        expect( allLevel ).to.equal( Number.MAX_SAFE_INTEGER );
    } );

    it( '#setLevel("INFO") should return correct level', function () {
        logger.setLevel( 'INFO' );
        expect( logger.level ).to.equal( Logger.LogLevel.INFO.level );
    } );

    it( '#setLevel() with unknown category should return ALL level', function () {
        const result = logger.resolveLevel('XYZ');
        expect( result ).to.equal( Logger.LogLevel.ALL.level );
    } );

    it( '#setLevel("WARN", "custom-appender/my-json-appender") should return correct level', function () {
        //@todo
    } );

    it( '#setLevel( "WARN" ) should only append entries of type WARN or above', function () {
        logger.setLevel( 'WARN' );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.FATAL, logger.level ) );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.ERROR, logger.level ) );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.WARN, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.INFO, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.DEBUG, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.TRACE, logger.level ) );
    } );

    it( '#setLevel( "ERROR" ) should only append entries of type ERROR or above', function () {
        logger.setLevel( 'ERROR' );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.FATAL, logger.level ) );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.ERROR, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.WARN, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.INFO, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.DEBUG, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.TRACE, logger.level ) );
    } );

    it( '#setLevel( "OFF" ) should append nothing', function () {
        logger.setLevel( 'OFF' );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.FATAL, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.ERROR, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.WARN, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.INFO, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.DEBUG, logger.level ) );
        assert.isFalse( logger.shouldAppend( Logger.LogLevel.TRACE, logger.level ) );
    } );

    it( '#setLevel( "ALL" ) should append everything', function () {
        logger.setLevel( 'ALL' );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.FATAL, logger.level ) );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.ERROR, logger.level ) );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.WARN, logger.level ) );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.INFO, logger.level ) );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.DEBUG, logger.level ) );
        assert.isTrue( logger.shouldAppend( Logger.LogLevel.TRACE, logger.level ) );
    } );

} );