'use strict';

const chai = require( 'chai' );
const assert = chai.assert;
const expect = chai.expect;
const sinon = require( 'sinon' );

const Logger = require( '../bin' );

describe( 'Basics', function() {

    const logger = new Logger();

    beforeEach( function () {
        sinon.spy( logger, 'trace' );
        sinon.spy( logger, 'info' );
        sinon.spy( logger, 'warn' );
        sinon.spy( logger, 'error' );
        sinon.spy( logger, 'debug' );
        sinon.spy( logger, 'setLevel' );
    } );

    afterEach( function () {
        logger.trace.restore();
        logger.info.restore();
        logger.warn.restore();
        logger.error.restore();
        logger.debug.restore();
        logger.setLevel.restore();
    } );

    it( 'Logger.LogLevel.TRACE should exist', function() {
        expect( Logger.LogLevel.INFO ).to.have.keys( 'level', 'text' );
    } );

    it( 'Logger.LogLevel.DEBUG should exist', function() {
        expect( Logger.LogLevel.DEBUG ).to.have.keys( 'level', 'text' );
    } );

    it( 'Logger.LogLevel.INFO should exist', function() {
        expect( Logger.LogLevel.INFO ).to.have.keys( 'level', 'text' );
    } );

    it( 'Logger.LogLevel.WARN should exist', function() {
        expect( Logger.LogLevel.WARN ).to.have.keys( 'level', 'text' );
    } );

    it( 'Logger.LogLevel.ERROR should exist', function() {
        expect( Logger.LogLevel.ERROR ).to.have.keys( 'level', 'text' );
    } );

    it( 'Logger.LogLevel.FATAL should exist', function() {
        expect( Logger.LogLevel.FATAL ).to.have.keys( 'level', 'text' );
    } );

    it( 'Logger.LogLevel.OFF should exist', function() {
        expect( Logger.LogLevel.OFF ).to.have.keys( 'level', 'text' );
    } );

    it( 'Logger.LogLevel.ALL should exist', function() {
        expect( Logger.LogLevel.ALL ).to.have.keys( 'level', 'text' );
    } );

    it( '#trace("Hello, World!") should be called', function() {
        logger.trace( 'Hello, World!' );
        assert( logger.trace.firstCall.calledWith( 'Hello, World!' ) );
    } );

    it( '#info("Hello, World!") should be called', function() {
        logger.info( 'Hello, World!' );
        assert( logger.info.firstCall.calledWith( 'Hello, World!' ) );
    } );

    it( '#debug() should be called', function() {
        logger.debug( 'Hello, World!' );
        assert( logger.debug.firstCall.calledWith( 'Hello, World!' ) );
    } );

    it( '#warn() should be called', function() {
        logger.warn( 'Hello, World!' );
        assert( logger.warn.firstCall.calledWith( 'Hello, World!' ) );
    } );

    it( '#error() should be called', function() {
        logger.error( 'Hello, World!' );
        assert( logger.error.firstCall.calledWith( 'Hello, World!' ) );
    } );

    it( 'should support multiple arguments', function() {
        logger.info( 'Hello', 'World', '!' );
        assert.equal( 'Hello World !', logger.info.getCall( 0 ).args.join( ' ' ) );
    } );

} );