'use strict';

const chai = require( 'chai' );
const assert = chai.assert;
const expect = chai.expect;
const fs = require( 'fs' );

const Logger = require( '../bin' );

describe( 'Log layout', function () {

    const layout1 = "%d - %c - %m";
    const layout2 = "%d | %c | %m";

    it( 'should be logged with given layout configuration', function () {
        const logger = new Logger( {
            "layout": layout1
        } );
        const parsed = logger.parseLayout( layout1, logger.date_format, Logger.LogLevel.DEBUG, 'Hello, World 1!' );
        assert.match( parsed, /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3} - DEBUG - Hello, World 1!/, 'regexp matches' );
    } );

    it( 'should be logged with given appender layout configuration', function () {
        const logger = new Logger(
            {
                "layout": layout1,
                "appenders": [
                    {
                        "type": "appender/console",
                        "layout": layout2
                    }
                ]
            }
        );

        const parsed = logger.parseLayout( layout2, logger.date_format, Logger.LogLevel.INFO, 'Hello, World 2!' );
        assert.match( parsed, /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3} \| INFO \| Hello, World 2!/, 'regexp matches' );
    } );

    it( 'should be logged with given layout configuration from #setLayout', function () {
        const logger = new Logger(
            {
                "layout": layout1,
                "appenders": [
                    {
                        "type": "appender/console",
                    }
                ]
            }
        );

        logger.setLayout(layout2, 'appender/console');

        const parsed = logger.parseLayout( layout2, logger.date_format, Logger.LogLevel.TRACE, 'Hello, World 2!' );
        assert.match( parsed, /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3} \| TRACE \| Hello, World 2!/, 'regexp matches' );
    } );

} );