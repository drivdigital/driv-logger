'use strict';

const chai = require( 'chai' );
const assert = chai.assert;
const expect = chai.expect;
const sinon = require( 'sinon' );
const fs = require( 'fs' );
const Logger = require( '../bin' );

describe( 'Appender', function () {

    const fileAppenderConfig = {
        "type": "appender/file",
        "options": {
            "path": "test/driv-logger-test.log"
        }
    };

    describe( 'API', function () {
        it( '#loadAppender() should load an appender', function () {
            const logger = new Logger();
            logger.loadAppender( fileAppenderConfig );

            assert.deepEqual( logger.getAppenderConfig(),
                [
                    {
                        "type": "appender/console"
                    },
                    {
                        "type": "appender/file",
                        "options": {
                            "path": "test/driv-logger-test.log"
                        }
                    }
                ]
            );
        } );

        it( '#loadAppender() should not load an appender more than once', function () {

            const logger = new Logger();

            logger.loadAppender( fileAppenderConfig );
            logger.loadAppender( fileAppenderConfig );
            logger.loadAppender( fileAppenderConfig );

            assert.deepEqual( logger.getAppenderConfig(),
                [
                    {
                        "type": "appender/console"
                    },
                    {
                        "type": "appender/file",
                        "options": {
                            "path": "test/driv-logger-test.log"
                        }
                    }
                ]
            );
        } );

        it( '#removeAppender() should remove an appender of given type', function () {
            const logger = new Logger();

            logger.loadAppender( fileAppenderConfig );

            logger.removeAppender( 'appender/file' );

            assert.deepEqual( logger.getAppenderConfig(),
                [
                    {
                        "type": "appender/console"
                    }
                ]
            );
        } );

        it( '#appenderIsLoaded() should return a boolean', function () {
            const logger = new Logger();
            assert.isTrue( logger.appenderIsLoaded( 'appender/console' ) );
            assert.isFalse( logger.appenderIsLoaded( 'appender/file' ) );
        } );
    } );

    describe( 'Custom appender', function () {
        const logger = new Logger( {
            "appenders": [
                {
                    "type": "custom-appender/my-json-appender",
                    "layout": '[%c] %d - %m',
                    "date_format": 'YYYY',
                    "options": {
                        "data": {
                            "file": __filename
                        }
                    }
                }
            ]
        } );

        beforeEach( function () {
            sinon.spy( logger, 'info' );
        } );

        afterEach( function () {
            logger.info.restore();
        } );

        it( 'should be called thrice', function () {
            logger.info( 'First call' );
            logger.info( 'Second call' );
            logger.info( 'Third call' );
            assert( logger.info.calledThrice );
        } );

    } );

    describe( 'File appender', function () {

        const logger = new Logger();
        const logFilePath = fileAppenderConfig.options.path;

        after( function () {
            fs.unlinkSync( logFilePath );
        } );

        it( 'log file should be created', function () {
            logger.loadAppender( fileAppenderConfig );
            logger.info( 'Hello, World!' );
            assert.isTrue( fs.existsSync( logFilePath ) );
        } );

        it( 'log file should have one entry', function () {
            const contents = fs.readFileSync( logFilePath, 'utf8' );
            assert.include( contents, 'Hello, World!', 'Should contain Hello, World!' );
        } );

        it( 'log file should end with new line', function () {
            const contents = fs.readFileSync( logFilePath, 'utf8' );
            assert( /\n$/.test( contents ), 'Should end with newline character' );
        } );
    } );

} );