'use strict';

const chai = require( 'chai' );
const assert = chai.assert;
const expect = chai.expect;
const sinon = require( 'sinon' );

const Logger = require( '../bin' );

describe( 'Log arguments', function() {

    const logger = new Logger();

    it( 'should stringify object', function() {
        assert.equal( '{"a":1,"b":2,"c":3}', logger.parseMessage( {
            a: 1,
            b: 2,
            c: 3
        } ) );
    } );

    it( 'should stringify array', function() {
        assert.equal( '[1,2,3]', logger.parseMessage( [1, 2, 3] ) );
    } );

} );