'use strict';

function myJsonAppender( message, options ) {
    let o = {
        message: message,
        data: options.data,

    };
    console.log( JSON.stringify( o ) );
}

exports.appender = myJsonAppender;