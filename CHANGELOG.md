Version 2.0.0 (2016-xx-xx)
  * Added OFF level. Nothing will be logged.
  * Added FATAL level.
  * Added TRACE level.
  * Added setLevel() Set log level. See README for how to use.
  * Added support for custom appenders. See README for how to use.
  * Added loadAppender({}) and removeAppender(name) in order to dynamically add/remove appender.
  * Replace moment.js with fecha. More lightweight.

Version 1.0.1 (2016-08-22)
  * Added license file.
  * Added changelog.
  * Added support for logging objects.
  * Added support for multiple arguments.
  * Removed file appender in default config.
  * Minor change to default layout.

Version 1.0.0 (2016-08-21)
  * Initial release.  