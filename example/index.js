'use strict';

const Logger = require( '../bin' );
const options = require( './sample.options.json' );

const logger = new Logger( options );

logger.trace( 'Prima probatus iudicabit his cu' );
logger.debug( 'Prima probatus iudicabit his cu' );
logger.info( 'Lorem ipsum dolor sit amet, his et nemore dolores' );
logger.warn( 'Prima probatus iudicabit his cu' );
logger.error( 'Prima probatus iudicabit his cu' );
logger.fatal( 'Prima probatus iudicabit his cu' );
logger.debug( 'Nested object', {a: [1, 2, 3], b: {c: 3, d: 4}} );

logger.info( [1, 2, 3] );
logger.info( 'test', [1, 2, 3], [4, 5, 6] );
logger.info( 'test', 'again' );
logger.info( {a: [1, 2, 3], b: {c: 3, d: 4}} );

logger.info( 'Set level to WARN' );
logger.setLevel( 'WARN' );

logger.info( 'Will not be logged' );
logger.debug( 'Will not be logged' );
logger.warn( 'Will be logged' );
logger.error( 'Will be logged' );