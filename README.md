# Driv Logger

Lightweight customizable logger utility for node applications inspired by log4js-node.

Requires node version 6 or above.

Features:

* Built-in console appender and file appender
* Custom appenders
* Log levels
* Layout formatting

## Installation

`npm install driv-logger --save`

## Usage

```javascript
const Logger = require('driv-logger');

const logger = new Logger();

logger.trace( 'Prima probatus iudicabit his cu' );
logger.debug( 'Prima probatus iudicabit his cu' );
logger.info( 'Lorem ipsum dolor sit amet, his et nemore dolores' );
logger.warn( 'Prima probatus iudicabit his cu' );
logger.error( 'Prima probatus iudicabit his cu' );
logger.fatal( 'Prima probatus iudicabit his cu' );
logger.debug( 'Nested object', {a: [1, 2, 3], b: {c: 3, d: 4}} );
```

Output:

```
[2016-09-05T15:57:48.665] [TRACE] Prima probatus iudicabit his cu
[2016-09-05T15:57:48.685] [DEBUG] Prima probatus iudicabit his cu
[2016-09-05T15:57:48.685] [INFO] Lorem ipsum dolor sit amet, his et nemore dolores
[2016-09-05T15:57:48.686] [WARNING] Prima probatus iudicabit his cu
[2016-09-05T15:57:48.686] [ERROR] Prima probatus iudicabit his cu
[2016-09-05T15:57:48.686] [FATAL] Prima probatus iudicabit his cu
[2016-09-05T15:57:48.686] [DEBUG] Nested object {"a":[1,2,3],"b":{"c":3,"d":4}}
```

## Options

Override the default behavior with passing an option object to the constructor.

Default options:

```javascript
{
  "level": "ALL",
  "date_format": "YYYY-MM-DDTHH:mm:ss.SSS",
  "layout": "[%d] [%c] %m",
  "appenders": [
    {
      "type": "appender/console"
    }
  ]
}
```

`level` - The log level.
`date_format` - The date format used for a log message.
`layout` - The layout of the log message.
`appenders` - List of appenders used for output.

### Log level

Available log levels

<table class="table table-striped table-bordered">
  <tbody>
    <tr>
      <th>Level</th>
      <th>Description</th>
    </tr>
    <tr>
      <td>OFF</th>
      <td>Nothing is logged</th>
    </tr>
    <tr>
      <td>FATAL</th>
      <td>Fatal errors are logged</th>
    </tr>
    <tr>
      <td>ERROR</th>
      <td>Errors are logged</th>
    </tr>
    <tr>
      <td>WARN</th>
      <td>Warnings are logged</th>
    </tr>
    <tr>
      <td>INFO</th>
      <td>Infos are logged</th>
    </tr>
    <tr>
      <td>DEBUG</th>
      <td>Debug infos are logged</th>
    </tr>
    <tr>
      <td>TRACE</th>
      <td>Traces are logged</th>
    </tr>
    <tr>
      <td>ALL</th>
      <td>Everything is logged</th>
    </tr>
  </tbody>
</table>

The levels are cumulative. If you for example set the logging level to WARN all warnings, errors and fatals are logged. 

Manually set log level:

```javascript
logger.setLevel('WARN');
```

### Appenders

Appenders are responsible for delivering messages to their destination.
Eg. `appender/console` delivers the message to the console. 

The core has two built-in appender types:

* appender/console
* appender/file

`appender/console` is the default.

`appender/file` example:

```javascript
const logger = new Logger( {
  "appenders": [
    {
      "type": "appender/file",
      "options": {
        "path: "path/to/file.log"
      }
    }
  ]  
} );
```

Note the `options.path`, this is required and specifies the file to write to.
The path can be absolute or relative to the main module's folder.

Multiple appenders example:

```javascript
const logger = new Logger( {
  "appenders": [
    {
      "type": "appender/console"
    },
    {
      "type": "appender/file",
      "options": {
        "path: "path/to/file.log"
      }
    }
  ]  
} );
```

Manually load an appender:

```javascript
const logger = new Logger(); // Default appender is 'console'

// Load file appender 
logger.loadAppender(
  {
    "type": "appender/file",
    "options": {
      "path: "path/to/file.log"
    }
  }
)
```

Manually remove an appender by type:

```javascript
...
logger.removeAppender('appender/file');
```

Appender specification:

```javascript
{
  "type": "appender/file",
  "level: "WARN",
  "layout: "%d - %m",
  "date_format": "YYYY-MM-DD",
  "options": {
    "path": "path/to/file.log"
  }
}
```

Default `level`, `layout` and `date_format` can be overridden in the appender configuration. 

### Custom appenders
 
A custom appender is a node js file that exports one function named `appender`

The function takes two arguments `message` and `options`

`message` is the message string.
`options` Optional. custom options for the appender.

Example:

```javascript
'use strict';

/**
 * @param {String} message
 * @param {Object} options
 */
function myAppender( message, options ) {
    console.log( message + ' ' + options.append_string );
} 
 
exports.appender = myAppender;
```

Usage:

```javascript
const logger = new Logger( {
  "type": "relative/path/to/my-appender",
  "options": {
    "append_string": "Hello from custom appender"
} );

```

### Layout

Layout can either be set globally or pr. appender.  
The following tokens are available:

*  %d - date
*  %c - log level text
*  %h - hostname
*  %m - message

Layout example:

```javascript
const logger = new Logger( {
  "layout": "❤ %c ❤ %d : %m",
} );

logger.info('Hello, World!');
```

Output:

```
❤ INFO ❤ 2016-09-04T14:24:41.526 : Hello, World!
```

### Date format

As with layout, the date format can either be set globally or pr. appender.  

Date format example:

```javascript
const logger = new Logger( {
  "date_format": "YYYY-MM-DD"
} );

logger.info('Hello, World!');
```

Output: 

```
[2016-09-04] [INFO] Hello, World!
```

driv-logger uses fecha for date formatting.

The following formatting tokens are available:

<table class="table table-striped table-bordered">
  <tbody>
    <tr>
      <th></th>
      <th>Token</th>
      <th>Output</th>
    </tr>
    <tr>
      <td><b>Month</b></td>
      <td>M</td>
      <td>1 2 ... 11 12</td>
    </tr>
    <tr>
      <td></td>
      <td>MM</td>
      <td>01 02 ... 11 12</td>
    </tr>
    <tr>
      <td></td>
      <td>MMM</td>
      <td>Jan Feb ... Nov Dec</td>
    </tr>
    <tr>
      <td></td>
      <td>MMMM</td>
      <td>January February ... November December</td>
    </tr>
    <tr>
      <td><b>Day of Month</b></td>
      <td>D</td>
      <td>1 2 ... 30 31</td>
    </tr>
    <tr>
      <td></td>
      <td>Do</td>
      <td>1st 2nd ... 30th 31st</td>
    </tr>
    <tr>
      <td></td>
      <td>DD</td>
      <td>01 02 ... 30 31</td>
    </tr>
    <tr>
      <td><b>Day of Week</b></td>
      <td>d</td>
      <td>0 1 ... 5 6</td>
    </tr>
    <tr>
      <td></td>
      <td>ddd</td>
      <td>Sun Mon ... Fri Sat</td>
    </tr>
    <tr>
      <td></td>
      <td>dddd</td>
      <td>Sunday Monday ... Friday Saturday</td>
    </tr>
    <tr>
      <td><b>Year</b></td>
      <td>YY</td>
      <td>70 71 ... 29 30</td>
    </tr>
    <tr>
      <td></td>
      <td>YYYY</td>
      <td>1970 1971 ... 2029 2030</td>
    </tr>
    <tr>
      <td><b>AM/PM</b></td>
      <td>A</td>
      <td>AM PM</td>
    </tr>
    <tr>
      <td></td>
      <td>a</td>
      <td>am pm</td>
    </tr>
    <tr>
      <td><b>Hour</b></td>
      <td>H</td>
      <td>0 1 ... 22 23</td>
    </tr>
    <tr>
      <td></td>
      <td>HH</td>
      <td>00 01 ... 22 23</td>
    </tr>
    <tr>
      <td></td>
      <td>h</td>
      <td>1 2 ... 11 12</td>
    </tr>
    <tr>
      <td></td>
      <td>hh</td>
      <td>01 02 ... 11 12</td>
    </tr>
    <tr>
      <td><b>Minute</b></td>
      <td>m</td>
      <td>0 1 ... 58 59</td>
    </tr>
    <tr>
      <td></td>
      <td>mm</td>
      <td>00 01 ... 58 59</td>
    </tr>
    <tr>
      <td><b>Second</b></td>
      <td>s</td>
      <td>0 1 ... 58 59</td>
    </tr>
    <tr>
      <td></td>
      <td>ss</td>
      <td>00 01 ... 58 59</td>
    </tr>
    <tr>
      <td><b>Fractional Second</b></td>
      <td>S</td>
      <td>0 1 ... 8 9</td>
    </tr>
    <tr>
      <td></td>
      <td>SS</td>
      <td>0 1 ... 98 99</td>
    </tr>
    <tr>
      <td></td>
      <td>SSS</td>
      <td>0 1 ... 998 999</td>
    </tr>
    <tr>
      <td><b>Timezone</b></td>
      <td>ZZ</td>
      <td>
        -0700 -0600 ... +0600 +0700
      </td>
    </tr>
  </tbody>
</table>

